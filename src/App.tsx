import './App.css';
import BoardComponent from './components/board'
import * as boardAlgo from './algo'

function App() {

  return (
    <BoardComponent board={boardAlgo.board} />
  );
}

export default App;
