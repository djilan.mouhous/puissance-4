type Player = 1 | 2;
type Cell = Player | null;
type Row = Cell[];
type Board = Row[];

const board: Board = [
  [null, null, null,null, null, null, null],
  [null, null, null,null, null, null, null],
  [null, null, null,null, null, null, null],
  [null, null, null,null, null, null, null],
  [null, null, null,null, null, null, null],
  [null, null, null,null, null, null, null]
];


/**
 * Return true if the board is full without a winner
 * @param board the current board
 */
const isDraw = (board: Board) => {
  let count = 0;
  board.forEach((element, index) => {
    element.forEach((element2, index2) => {
      if (element2) count++;
    });
  });
  return count === 42 ? true : false;
};


/**
 * Make an array with all values equal to null
 * @param length length of the created array
 */
function makeEmptyArray(length: number) {
  return Array(length).fill(null)
}

/**
 * Takes a list of cells and return the owner of 4 consecutive cells.
 * @param cells
 */
function getOwnerOf4(cells: Cell[]): Player | null {
  const [owner, length] = cells.reduce<[Player | null, number]>(
    ([previousCell, length], cell) =>
      length >= 4
        ? // if we have an owner, just pass it along
          [previousCell, length + 1]
        : cell && cell === previousCell
        ? // if we dont but cells matches, pass along and increment
          [previousCell, length + 1]
        : cell
        ? // if cells don't match but cells isn't null, start at 1
          [cell, 1]
        : // if cells don't match but cells and cell is null, start at 0
          [cell, 0],
    [null, 0],
  )

  return length >= 4 ? owner : null
}

/**
 * Extract all the diagonals from a board.
 * @param board
 */
function getDiagonals(board: Board): Cell[][] {
  const rowsLength = board.length

  const topLeftDiags: Cell[][] = []
  const botLeftDiags: Cell[][] = []
  const topRightDiags: Cell[][] = []
  const botRightDiags: Cell[][] = []

  for (let y = 0; y < rowsLength; y++) {
    const topLeftDiag: Cell[] = []
    const topRightDiag: Cell[] = []
    const botLeftDiag: Cell[] = []
    const botRightDiag: Cell[] = []

    for (let x = 0; x <= y; x++) {
      topLeftDiag.push(board[y - x][x])
      topRightDiag.push(board[y - x][rowsLength - x])
      botLeftDiag.push(board[rowsLength - y - 1 + x][x])
      botRightDiag.push(board[rowsLength - y - 1 + x][rowsLength - x])
    }

    topLeftDiags.push(topLeftDiag)
    topRightDiags.push(topRightDiag)
    botLeftDiags.push(botLeftDiag)
    botRightDiags.push(botRightDiag)
  }

  return [...topLeftDiags, ...botLeftDiags, ...topRightDiags, ...botRightDiags]
}

/**
 * Extract all the columns from a board.
 * This basically flips the board (`board[y][x]` becomes `board[x][y]`).
 * @param board
 */
function getColumns(board: Board): Cell[][] {
  return makeEmptyArray(board[0].length).map((_, y) =>
    makeEmptyArray(board.length).map((_, x) => board[x][y]),
  )
}

/**
 * Get the winnner of a board, if there is one.
 * @param board
 */
function getWinner(board: Board): Player | null {
  const rows = board
  const columns = getColumns(board)
  // diagonals smaller than 4 can't determine a winner so we remove them
  const diagonals = getDiagonals(board).filter((cells) => cells.length >= 4)

  return [...rows, ...columns, ...diagonals].reduce<Player | null>(
    (winner, cells) => winner || getOwnerOf4(cells),
    null,
  )
}

/**
 * Define if the current player is the player 1
 * @param board the current board
 */
const isFirstPlayerTurn = (board: Board): boolean => {
  const emptyCellCount = board[0]
    .concat(board[1], board[2], board[3], board[4], board[5], board[6])
    .filter((cell) => cell === null).length;
  return emptyCellCount % 2 === 1;
};

/**
 * Play a round of the game (try to place a coin)
 * Will return the updated board
 * @param board the current board
 * @param col the column of the cell selected by the player
 * @param row the row of the cell selected by the player
 */
const play = (board: Board, col: number, row: number) => {
  if (getWinner(board) === null && board[row][col] === null) {
    for (let index = 0; index < board.length; index++) {
      if (board[index + 1] !== undefined && board[index + 1][col] !== null){
        board[index][col] = isFirstPlayerTurn(board) ? 2 : 1;
        break
      } else if (board[index + 1] === undefined && board[index][col] === null){
        board[index][col] = isFirstPlayerTurn(board) ? 2 : 1;
        break
      } 
      
    }
    } else if (getWinner(board) !== null) {
  }
  return board;
};

/**
 * The function that will be fired when the user click on a cell
 * Will return the updated board
 * @param element the current cell value
 * @param board the current board
 * @param col the column of the cell selected by the player
 * @param row the row of the cell selected by the player
 */
function handleClick (element : Cell | null, board : Board, col : number, row: number){
  let board2 : Board = board.slice();
    if (!element && !getWinner(board2)) {
         board2 = play(board2, col, row);
        if (getWinner(board2)) {
        } else if (isDraw(board2)) {
        } 
    };
    return board2
 }

export {makeEmptyArray, isDraw, isFirstPlayerTurn, getWinner, handleClick, board};
export type { Player, Cell, Row, Board };

