import * as boardAlgo from '../algo'

function Cell(props: { cell: boardAlgo.Cell, setParentData : (board: boardAlgo.Board) => void, defaultBoard : boardAlgo.Board, col : number, row : number, setHoverIndicator : (col: number) => void}){

    //DEFINE THE COLOR USING THE VALUE OF THE CELL
    const color =  props.cell ? ( props.cell === 1 ? "red" : "yellow") : ""
    return ( 
        <div data-col={props.col + 1} data-row={props.row + 1} className={'cell ' + color } onMouseOver={()=>props.setHoverIndicator(props.col)}  onClick={() => {props.setParentData(boardAlgo.handleClick(props.cell, props.defaultBoard, props.col, props.row));}}>
                        
        </div>
        
    )
}
export default Cell