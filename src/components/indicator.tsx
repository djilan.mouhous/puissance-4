import { board } from "../algo"

import { HiChevronDoubleDown } from "react-icons/hi";

function Indicator (props: {col : number, currentTurn : number}){
    const listIndicators = board[0].map((cell, index) =>{
        const indicatorColor = props.col === index ? (props.currentTurn === 1 ? ' currentCol fillRed' : ' currentCol fillYellow') : '' //Here we define wich column is the current and his color
        return(
            <div key={index} className={"indicator" + indicatorColor}>
                <HiChevronDoubleDown key={index+'chevron'} />
            </div>
        )
    }
    )
    return (
        <div className="indicatorWrapper">
            {listIndicators}
        </div>
    )
}
export default Indicator