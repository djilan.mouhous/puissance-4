import * as boardAlgo from '../algo'
import { useState } from 'react';
import Cell from './cell';
import React from 'react';
import Confetti from 'react-confetti' //Propulsed by https://github.com/alampros/react-confetti
import Indicator from './indicator';

function BoardComponent(props: { board: boardAlgo.Board}) {

    /*
     * We are going to declare our differents state here
     * Board is going to receive the value of our board
     * Turn is going to reveive the current player turn (1 || 2)
     * Score 1 & Score 2 are the score of the players
     * Disable will help us to know wheter the game is over or not
     */
    const [board, setBoard] = useState(props.board);
    const [turn, setTurn] = useState(1)
    const [score1, setScore1] = useState(0)
    const [score2, setScore2] = useState(0)
    const [disable, setDisable] = useState(false)
    const [currentCol, setCurrentCol] = useState(-1)

    /*
     * The function handleChange will be fired as a player click on a cell
     * It will receive the updated Board value and will define new states
     */
    function handleChange (newValue : boardAlgo.Board){ 
        if (!disable){ // This condition will help us to not do anything if the game is over 
            setBoard(newValue) // At first we update the board
            setTurn(boardAlgo.isFirstPlayerTurn(board)  ? 2 : 1) // Then we update the current turn
            let winner = boardAlgo.getWinner(board)
            winner ? (winner === 1 ? setScore1(score1 + 1) : setScore2(score2 + 1)) : setScore1(score1) //Now we can update the score if the game is won by a player
            winner ?  setDisable(true) : boardAlgo.isDraw(board) ? setDisable(true) : setDisable(false)  // We also disable the game if it is won
        }
    }
    function handleHover (newCol : number){
        !disable ? setCurrentCol(newCol) : setDisable(true)
    }

    /*
     * The function resetGame will be fired as a player click on the reset button
     * It will reset all states except scores
     */
    function resetGame() {
        setBoard(board.map((row) => row = boardAlgo.makeEmptyArray(row.length))) // Clear the board
        setTurn(1)
        setDisable(false)
    }

    return (
        <section>
            <div className={`myBoard ${disable ? 'disabled' : ''}`}>
                <div className="blockTop"></div>
                {!disable ?
                    <Indicator col={currentCol} currentTurn={turn} />
                    :
                    ""
                }
                    {board.map((row: boardAlgo.Row, indexRow: number) => // For each row we generate an element
                        <div key={indexRow} className="row">
                            {row.map((cell: boardAlgo.Cell, indexCol: number) => { // For each cell we generate a Cell component
                                return (
                                    <Cell key={indexCol+'_'+indexRow+'_'+cell} cell={cell}  defaultBoard={board} row={indexRow} col={indexCol} setParentData={handleChange} setHoverIndicator={handleHover} />
                                )
                            })}
                        </div>
                    )}
            </div>
            <div className="infos">
                <h1>Puissance 4</h1>
                    {disable ? //Board is disabled, game is over
                        (boardAlgo.getWinner(board) ? //If we can get a winner from the board
                                [ 
                                    <Confetti recycle={false}/> , //We put some fancy confetti 
                                    <h2><span className={boardAlgo.getWinner(board) === 1 ? "colorRed" : "colorYellow"} >Joueur {boardAlgo.getWinner(board)}</span> a gagné !</h2> // And the name of the winner
                                ]
                            : //If we can't get a winner that means there is a draw
                                <h2>Partie terminée : Égalité</h2>
                        ): //Board is not disabled, game is on
                        <h2><span className={turn === 1 ? "colorRed" : "colorYellow"} >Joueur {turn}</span> à ton tour !</h2>
                    }
                <div className="joueurWrapper">
                    <span className="joueur colorRed">Joueur 1 : {score1}</span>
                    <span className="joueur colorYellow">Joueur 2 : {score2}</span>
                </div>
                <button onClick={resetGame} >RECOMMENCER</button>
            </div>
        </section>
    );
}

export default BoardComponent;