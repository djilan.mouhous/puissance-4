# Puissance 4 en React !

🎮 Jeu de puissance 4

💻 Réalisé par Djilan Mouhous (3AWD)

📦 Utilisation de React rédigé en TypeScript

🔗 Aussi disponible directement en ligne sur [puissance4.djilan.fr](http://puissance4.djilan.fr)

## Démarer le projet

Pour commencer, cloner le projet à l'endroit souhaité 

`
$ git clone https://gitlab.com/djilan.mouhous/puissance-4/
`

Ensuite, se rendre dans le projet et installer les dépendances 

`
$ cd puissance-4
`

`
$ npm i
`

Enfin, on peut alors démarer le projet 

`
$ npm start
`

L'application est alors déployée sur [http://localhost:3000](http://localhost:3000)

## Principe de fonctionnement

### Les composants

Ce puissance 4 s'appuie sur deux composants :
1. **BoardComponent** : C'est le composant qui genère la grille
2. **Cell** : C'est le composant qui genère une case
3. **Indicator** : C'est le composant qui genère une flèche colorée au dessus de la colonne en survol

Il faut bien noter que le composant **Cell** est l'enfant de **BoardComponent**
Notre cellule (donc **Cell**) possède deux props qui sont des fonctions déclarées dans son parent **BoardComponent**.
Cela sert à faire remonter l'information depuis notre cellule jusqu'à son parent afin de le regénérer lorsqu'il y a une modification (onClick et OnMouseOver).

### Les états 

Le composant **BoardComponent** possède plusieurs états afin de générer une grille :
1. **_Board_** : Définit le contenu de notre grille
2. **_Turn_** : Définit l'utilisateur qui doit placer un jeton
3. **_Score1_** : Définit le score de l'utilisateur 1
4. **_Score2_** : Définit le score de l'utilisateur 2
5. **_Disable_** : Définit si la grille doit être desactivée
6. **_CurrentCol_** : Définit la colonne au survol

### Déroulement d'un tour
Au clic sur une cellule, la props de notre cellule est appelée en prenant en paramètre la nouvelle valeur de la grille.
Pour déterminer cette nouvelle valeur, nous avons créer une fonction _Play_ qui prends en paramètre la cellule choisie et la grille actuelle.

A partir de cela, notre fonction va boucler sur toutes les lignes de notres grille.
- Si la cellule du dessous existe et qu'elle est libre, alors on passe à l'itération suivante.
- Mais si la cellule du dessous n'existe pas ou qu'elle est déjà prise, on rentre notre jeton dans la cellule actuelle

Notre parent **BoardComponent** va alors recevoir la modification de l'état de la grille et va opérer les changements suivants :
1. L'état **_Board_** prends la nouvelle valeur reçue
2. L'état **_Turn_** est défini afin de passer au joueur suivant
3. L'état **_Disable_** prends la valeur de True si il y a un gagnant
4. L'état **_Score1_** ou **_Score2_** s'incrémente si l'un des deux à gagner

La modification de l'état **_Board_** entraine alors une regéneration du composant **BoardComponent** .

### Recommencer une partie

Au clic sur le bouton "Recommencer", la fonction resetGame est alors déclenchée.
Cette fonction va procéder au redemmarage de la partie (en conservant les scores) en réalisant les opérations suivantes :
1. Remettre l'état **_Board_** initial, c'est à dire un tableau de tableaux de nulls
2. Remettre l'état **_Turn_** initial, c'est à dire à 1
3. Remettre l'état **_Disable_** initial, c'est à dire à false

La modification de l'état **_Board_** entraine alors une regéneration du composant **BoardComponent** .
